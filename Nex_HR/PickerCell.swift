//
//  PickerCellTableViewCell.swift
//  Nex_HR
//
//  Created by Daud on 10/5/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class PickerCell: UITableViewCell {

    let lbl = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style : style, reuseIdentifier : reuseIdentifier)
        lbl.textAlignment = .left
        lbl.font = UIFont(name: "SFUIText-Medium", size: 20)
        self.addSubview(lbl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(_ title : String, tag : Int, color : Bool)
    {
        self.tag = tag
        lbl.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        lbl.text = title
        if(color)
        {
            lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
        }
        else
        {
            lbl.textColor = UIColor.black
        }
    }

}
