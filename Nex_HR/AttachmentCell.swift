//
//  AttachmentCell.swift
//  Nex_HR
//
//  Created by Daud on 11/9/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class AttachmentCell: UITableViewCell, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let lbl = UILabel()
    let btn = UIButton()
    
    var vc : ViewController!
    var parentView = ""
    var cellNum = 0
    
    let picker = UIImagePickerController()
    let imgView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style : style, reuseIdentifier : reuseIdentifier)
        self.addSubview(lbl)
        self.addSubview(btn)
        self.addSubview(imgView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(lbl2 : String, btn2 : String, vc : ViewController, parentView : String, cellNum : Int)
    {
        self.vc = vc
        self.parentView = parentView
        self.cellNum = cellNum
        picker.delegate = self
        
        lbl.frame.size.width = 1000
        lbl.text = lbl2
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "SFUIText-Medium", size: 16)
        lbl.sizeToFit()
        lbl.frame.origin.x = 0
        lbl.center.y = self.frame.height / 2
        
        btn.setTitle("Choose Image", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 15)
        btn.sizeToFit()
        btn.backgroundColor = UIColor(red: 190/255, green: 191/255, blue: 192/255, alpha: 1)
        btn.frame = CGRect(x: lbl.frame.width + 10, y: 0, width: btn.frame.width + 20, height: btn.frame.height + 5)
        btn.layer.cornerRadius = 5
        btn.center.y = self.frame.height / 2
        btn.addTarget(self, action: #selector(self.chooseDate(_:)), for: .touchUpInside)
        btn.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btn.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btn.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
        
        imgView.frame = CGRect(x: self.frame.width - 50, y: 0, width: 50, height: 50)
        imgView.contentMode = .scaleAspectFill
        imgView.center.y = self.frame.height / 2
        imgView.backgroundColor = UIColor.clear
        imgView.layer.cornerRadius = 2
        imgView.clipsToBounds = true
        imgView.layer.borderColor = UIColor.gray.cgColor
        imgView.layer.borderWidth = 0
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.chooseDate(_:))))
        
    }
    
    func chooseDate(_gesture : UIGestureRecognizer)
    {
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        vc.present(picker, animated : true, completion : nil)
    }
    
    func chooseDate(_ btn : UIButton)
    {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        vc.present(picker, animated : true, completion : nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
       
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            imgView.image = pickedImage
            imgView.layer.borderWidth = 1
            
            if parentView == "leaveView"
            {
                vc.leaveFormView.choosenImage = pickedImage
            }
            else if parentView == "overtimeView"
            {
                vc.overtimeFormView.choosenImage = pickedImage
            }
            
        }
        
//        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
        
            
//        }
        vc.dismiss(animated:true, completion: nil)
    }
    

}
