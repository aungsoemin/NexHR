//
//  ProfileView.swift
//  Nex_HR
//
//  Created by Daud on 9/28/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class ProfileView: UIScrollView, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let sideAlignment : CGFloat = 20
    let lbl1 : [String] = ["Name","E.code","Department","Bank Acc","Position","Gender","Date of Birth","NRC","Address","Phone","Emergency Contact","Ser.Length","Salary"]
    var lbl2 : [String] = ["Zayer Zy","nex.001","Development","0091321234","Head of Projects","Male","01.01.1990","12 / BHN ( Naing ) 000000","No.9, Kyaung St, Sanchaung, ygn","09123456878","Daw Khin Thi Thi","50 Months","$$$$$$$$"]
    var constraint = NSLayoutConstraint()
    
//    let btnProfile = UIButton()
    let tabView = UITableView()
    let scrView = UIScrollView()
    var coverPhoto = UIImageView()
//    var profileContainer = UIImageView()
    var profileImage = UIImageView()
    
    let headerCover = UIView()
    let lblName = UILabel()
    let lblEmail = UILabel()
    
    var cellHeights = [CGFloat(40),CGFloat(40)]
    var emerNameList = [""]
    var emerPhoneList = [""]
    var phoneList = [""]
    var imageStatus = ""
    
    var vc : ViewController!
    
    
    func initialize(_ vc : ViewController, btnProfileY : CGFloat)
    {
        self.vc = vc
//        self.backgroundColor = UIColor.white
//        
        updateInformation()
        
        coverPhoto.frame = CGRect(x: 0, y: -80, width: self.frame.width, height: 240)
        coverPhoto.contentMode = .scaleAspectFill
        coverPhoto.clipsToBounds = true
        coverPhoto.isUserInteractionEnabled = true
        coverPhoto.alpha = 0
        coverPhoto.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.chooseCoverPhoto(_:))))
        scrView.addSubview(coverPhoto)
        
        profileImage.frame = vc.homeView.coverPhoto.frame
        profileImage.contentMode = .scaleAspectFill
        profileImage.clipsToBounds = true
        profileImage.isUserInteractionEnabled = true
        profileImage.alpha = 0
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.chooseProfilePhoto(_:))))
        profileImage.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        profileImage.center = CGPoint(x: self.frame.width / 2, y: self.vc.homeView.coverPhoto.frame.height / 3)
        scrView.addSubview(profileImage)
//        profileContainer.addSubview(profileImage)
        
        let userDefault = UserDefaults.standard
        if (userDefault.data(forKey: "coverImage") == nil)
        {
            coverPhoto.image = UIImage(named: "imgLoadPhoto.jpg")
        }
        else
        {
            coverPhoto.image = UIImage(data: userDefault.data(forKey: "coverImage")!)
        }
        
        if (userDefault.data(forKey: "profileImage") == nil)
        {
            profileImage.image = UIImage(named: "imgLoadPhoto.jpg")
        }
        else
        {
            profileImage.image = UIImage(data: userDefault.data(forKey: "profileImage")!)
        }
//        coverPhoto.image = UIImage(named: "imgLoadPhoto.jpg")
        
        tabView.dataSource = self
        tabView.delegate = self
        tabView.register(CaptionedTextBoxCell.self, forCellReuseIdentifier: "cellType1")
        tabView.register(CaptionedEditabledCell.self, forCellReuseIdentifier: "captionedEditableCell")
        tabView.frame = CGRect(x: sideAlignment, y: 330, width: self.frame.width - sideAlignment * 2, height: 90 * 13)
        tabView.separatorColor = UIColor.clear
        tabView.alpha = 0
        scrView.addSubview(tabView)
        
        scrView.frame = self.frame
        scrView.delegate = self
        self.addSubview(scrView)
        
        lblName.frame = CGRect(x: sideAlignment, y: coverPhoto.frame.height - 75, width: 100, height: 20)
        lblEmail.frame = CGRect(x: sideAlignment, y: coverPhoto.frame.height - 35, width: 100, height: 20)
        lblName.text = lbl2[0]
        lblEmail.text = vc.userInform.email
        lblName.textColor = UIColor.black
        lblEmail.textColor = UIColor.black
        lblName.font = UIFont(name: "SFUIText-Bold", size: 32)
        lblEmail.font = UIFont(name: "SFUIText-Medium", size: 20)
        lblName.sizeToFit()
        lblEmail.sizeToFit()
        lblName.alpha = 1
        lblName.layer.shadowRadius = 10
        lblName.layer.shadowOpacity = 0
        lblEmail.alpha = 1
        lblEmail.layer.shadowRadius = 10
        lblEmail.layer.shadowOpacity = 0
        lblEmail.clipsToBounds = false
        scrView.addSubview(lblName)
        scrView.addSubview(lblEmail)
        
        headerCover.frame = CGRect(x: 0, y: -20, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 0
        self.addSubview(headerCover)
        
        let lblProfile = UILabel()
        lblProfile.text = "Profile"
        lblProfile.textColor = UIColor.black
        lblProfile.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblProfile.sizeToFit()
        lblProfile.frame.origin.x = sideAlignment
        lblProfile.center.y = headerCover.frame.height / 2
        headerCover.addSubview(lblProfile)
        
        let btnHome = UIButton(frame: CGRect(x: self.frame.width - 60 - sideAlignment, y: sideAlignment - 5 , width: 65, height: 65))
        btnHome.setImage(UIImage(named: "btnHome.png"), for: .normal)
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnHome.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        btnHome.alpha = 0
        btnHome.center.y = 10
        scrView.addSubview(btnHome)
        
        let btnHome2 = UIButton(frame: CGRect(x: self.frame.width - 35 - sideAlignment, y: sideAlignment , width: 35, height: 35))
        btnHome2.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome2.imageView?.contentMode = .scaleAspectFit
        btnHome2.center.y = headerCover.frame.height / 2
        btnHome2.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        headerCover.addSubview(btnHome2)
        
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = 0
        animation.toValue = 60
        animation.duration = 0.4
        self.profileImage.layer.add(animation, forKey: "cornerRadius")
        self.profileImage.layer.cornerRadius = 60
        
        self.profileImage.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        UIView.animate(withDuration: 0.4, delay: 0.2, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
            
            self.lblEmail.center.x = self.frame.width / 2
            self.lblName.center.x = self.frame.width / 2
//            self.profileContainer.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
//            self.profileContainer.layer.cornerRadius = self.profileContainer.frame.width / 2
//            self.profileImage.frame = self.profileContainer.frame
            //            self.profileContainer.center = CGPoint(x: self.frame.width / 2, y: self.vc.homeView.coverPhoto.frame.height / 3)
            self.profileImage.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.profileImage.alpha = 1
            self.tabView.frame.origin.y = 250
            self.tabView.alpha = 1
            
        }, completion: {finish in
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
                
                btnHome.center.y = 50
                btnHome.alpha = 1
                self.coverPhoto.frame.origin.y = 0
                self.coverPhoto.alpha = 1
                self.profileImage.center.y = self.vc.homeView.coverPhoto.frame.height
//                self.profileImage.alpha = 1
                self.lblEmail.frame.origin.y = self.lblEmail.frame.origin.y + 150
                self.lblName.frame.origin.y = self.lblName.frame.origin.y + 150
                self.tabView.frame.origin.y = 400
                
            }, completion: nil)
        })
        
        
//        updateInformation()
        tabView.frame.size.height = 90 * 11 + cellHeights[0] + cellHeights[1]
        scrView.contentSize.height = 350 + tabView.frame.height + 40
    }
    
    //Choose cover photo from album when pressed on cover photo
    func chooseCoverPhoto(_ gesture : UIGestureRecognizer)
    {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        imageStatus = "cover"
        vc.present(picker, animated : true, completion : nil)
    }
    
    //Choose cover photo from album when pressed on cover photo
    func chooseProfilePhoto(_ gesture : UIGestureRecognizer)
    {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        imageStatus = "profile"
        vc.present(picker, animated : true, completion : nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            let userDefault = UserDefaults.standard
            if imageStatus == "cover"
            {
                coverPhoto.image = pickedImage
                userDefault.set(UIImageJPEGRepresentation(pickedImage, 1) as NSData?, forKey: "coverImage")
            }
            else
            {
                profileImage.image = pickedImage
                userDefault.set(UIImageJPEGRepresentation(pickedImage, 1) as NSData?, forKey: "profileImage")
            }
        }
        vc.dismiss(animated:true, completion: nil)
    }

    
    func updateInformation()
    {
        lbl2[0] = vc.userInform.name
        lbl2[1] = vc.userInform.eCode
        lbl2[2] = vc.userInform.department
        lbl2[3] = vc.userInform.bankAcc
        lbl2[4] = vc.userInform.position
        lbl2[5] = vc.userInform.gender
        lbl2[6] = vc.userInform.doB
        lbl2[7] = vc.userInform.nrc
        lbl2[8] = vc.userInform.address
        lbl2[9] = ""
        lbl2[10] = ""
        phoneList = vc.userInform.phone
        emerNameList = vc.userInform.emerName
        emerPhoneList = vc.userInform.emerPhone
//        emerList = ["11","22","33","44"]
        lbl2[11] = vc.userInform.serLength
        lbl2[12] = vc.userInform.salary
        
        cellHeights[0] = CGFloat(35 + phoneList.count * 50) + 20
        cellHeights[1] = CGFloat(35 + emerNameList.count * 50) + 20
        
        print("cell height 0 = \(cellHeights[0])")
        print("cell height 1 = \(cellHeights[1])")
        
        if (cellHeights[0] < 90)
        {
            cellHeights[0] = 90
        }
        if (cellHeights[1] < 90)
        {
            cellHeights[1] = 90
        }
        tabView.frame.size.height = 90 * 11 + (cellHeights[0] + cellHeights[1])
        scrView.contentSize.height = 350 + tabView.frame.height + 40
    }
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 13
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 9)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "captionedEditableCell", for: indexPath as IndexPath) as! CaptionedEditabledCell
            cell.initialize(vc : vc, parentView : "Phone",lbl1: lbl1[indexPath.row], list : phoneList)
            cell.selectionStyle = .none
            return cell
        }
        else if (indexPath.row == 10)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "captionedEditableCell", for: indexPath as IndexPath) as! CaptionedEditabledCell
            cell.initialize(vc : vc, parentView : "Emergency", lbl1: lbl1[indexPath.row], emerName : emerNameList, emerPhone : emerPhoneList)
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellType1", for: indexPath as IndexPath) as! CaptionedTextBoxCell
            cell.initialize(lbl1[indexPath.row], lbl2 : lbl2[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 9 || indexPath.row == 10)
        {
            return cellHeights[indexPath.row - 9]
        }
        else
        {
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.endEditing(true)
        let scrOffset = scrView.contentOffset.y
        if (scrOffset < 0)
        {
            coverPhoto.frame.origin.y = scrOffset
            coverPhoto.frame.size.height = 240 + scrOffset * -1
        }
        if (scrOffset >= 110)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 1
                self.headerCover.frame.origin.y = 0
                
                }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 0
                self.headerCover.frame.origin.y = -20
                
                }, completion: nil)
        }
        
    }
    
    //Go Back to Home Screen--------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func goBackHome(_ btn : UIButton)
    {
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.frame.height
//            self.alpha = 0
            
            }, completion: {finish in
                
                UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                    
                    self.vc.homeView.lblEmail.alpha = 1
                    self.vc.homeView.lblName.alpha = 1
                    self.vc.homeView.coverPhoto.alpha = 1
                    self.vc.homeView.transform = CGAffineTransform(scaleX: 1, y: 1)
                    self.vc.homeView.alpha = 1
                    
                    }, completion: {finish in})
                    
                    self.removeFromSuperview()
                
        })
    }

    
}
