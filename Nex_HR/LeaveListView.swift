//
//  LeaveListView.swift
//  Nex_HR
//
//  Created by Daud on 10/2/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class LeaveListView: UITableViewCell {
    
    let greenView = UIView()
    let lblCasual = UILabel()
    let lblAvDate = UILabel()
    
//    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
//    {
//        super.init(style : style, reuseIdentifier : reuseIdentifier)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    func initialize(_ title : String, color : UIColor, total : Int, used : Int, endDate : [String], startDate : [String])
    {
        self.backgroundColor = UIColor.clear
        
        greenView.frame = CGRect(x: 0, y: 20, width: 18, height: 18)
        greenView.layer.cornerRadius = greenView.frame.width / 2
        greenView.backgroundColor = color
        
        lblCasual.frame = CGRect(x: 38, y: greenView.frame.origin.y, width: 100, height: 100)
        lblCasual.text = title
        lblCasual.textColor = UIColor.black
        lblCasual.font = UIFont(name: "SFUIText-Bold", size: 18)
        lblCasual.sizeToFit()
        
        lblAvDate.frame = CGRect(x: 38, y: greenView.frame.origin.y, width: 100, height: 100)
        
        if (total == -1)
        {
            lblAvDate.text = "\(used)/ -  days"
        }
        else
        {
            lblAvDate.text = "\(used)/\(total) days"
        }
        
        lblAvDate.textColor = UIColor.black
        lblAvDate.font = UIFont(name: "SFUIText-Medium", size: 15)
        lblAvDate.sizeToFit()
        lblAvDate.frame.origin.x = self.frame.width - lblAvDate.frame.width - 20
        
        for cc in self.subviews
        {
            cc.removeFromSuperview()
        }
        
        self.addSubview(greenView)
        self.addSubview(lblCasual)
        self.addSubview(lblAvDate)
        
        for var i in(0 ..< endDate.count)
        {
            let lblNum = UILabel(frame : CGRect(x: 38, y: 70 + (i * 40), width: 400, height: 100))
            lblNum.text = "\(i + 1)."
            lblNum.textColor = UIColor.black
            lblNum.font = UIFont(name: "SFUIText-Medium", size: 16)
            lblNum.sizeToFit()
            self.addSubview(lblNum)
            
            let lblStart = UILabel(frame : CGRect(x: Int(lblNum.frame.origin.x + lblNum.frame.width + 20), y: 70 + (i * 40), width: 400, height: 100))
            lblStart.text = startDate[i]
            lblStart.textColor = UIColor.black
            lblStart.font = UIFont(name: "SFUIText-Medium", size: 14)
            lblStart.sizeToFit()
            lblStart.alpha = 0.7
            self.addSubview(lblStart)
            
            if(endDate[i] != startDate[i])
            {
                let lblTo = UILabel(frame : CGRect(x: Int(lblStart.frame.origin.x + lblStart.frame.width + 10), y: 70 + (i * 40), width: 400, height: 100))
                lblTo.text = "to"
                lblTo.textColor = UIColor.black
                lblTo.font = UIFont(name: "SFUIText-Medium", size: 14)
                lblTo.sizeToFit()
                lblTo.alpha = 0.4
                self.addSubview(lblTo)
                
                let lblEnd = UILabel(frame : CGRect(x: Int(lblTo.frame.origin.x + lblTo.frame.width + 10), y: 70 + (i * 40), width: 400, height: 100))
                lblEnd.text = endDate[i]
                lblEnd.textColor = UIColor.black
                lblEnd.font = UIFont(name: "SFUIText-Medium", size: 14)
                lblEnd.sizeToFit()
                lblEnd.alpha = 0.7
                self.addSubview(lblEnd)
            }
            
        }
        
        let border = UIView(frame : CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: 1))
        border.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        self.addSubview(border)
        
    }

}
