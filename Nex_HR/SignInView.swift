//
//  SignInView.swift
//  Nex_HR
//
//  Created by Daud on 10/18/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class SignInView: UIView, UITextFieldDelegate, UIScrollViewDelegate {
    
    var vc : ViewController!
    var userInform : User_Inform!
    
    let sideAlignment : CGFloat = 40
    
    let txtEmail = UITextField()
    let txtPass = UITextField()
    let signInView = UIView()
    let btnSignIn = UIButton(type : .system)
    
    var leaveTypeJSON = [NSDictionary]()
    var statusJSON = [NSDictionary]()
    var approvedByJSON = [NSDictionary]()
    var reportToJSON = [NSDictionary]()
    
    var automatic = false
    
    var firstTime = false
    
    func initialize(vc : ViewController)
    {
        self.vc = vc
        self.userInform = vc.userInform
        self.backgroundColor = UIColor.clear
        firstTime = true
        
        let scrView = UIScrollView(frame: self.frame)
        scrView.delegate = self
        scrView.contentSize.height = scrView.frame.height + 1
        self.addSubview(scrView)
        
        signInView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 200)
        //signInView.backgroundColor = UIColor.red
        
        let lblSignIn = UILabel()
        lblSignIn.text = "Sign in .."
        lblSignIn.textColor = UIColor.black
        lblSignIn.font = UIFont(name: "SFUIText-Bold", size: 45)
        lblSignIn.sizeToFit()
        lblSignIn.frame.origin.x = sideAlignment
        lblSignIn.frame.origin.y = 0
        signInView.addSubview(lblSignIn)
        
        let lblToNex = UILabel()
        lblToNex.text = "to Nex"
        lblToNex.textColor = UIColor.black
        lblToNex.font = UIFont(name: "SFUIText-Bold", size: 35)
        lblToNex.sizeToFit()
        lblToNex.frame.origin.x = sideAlignment
        lblToNex.frame.origin.y = 50
        signInView.addSubview(lblToNex)
        
        txtEmail.frame = CGRect(x: sideAlignment, y: 110, width: self.frame.width - sideAlignment * 2, height: 40)
        txtEmail.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        txtEmail.font = UIFont(name: "SFUIText-Medium", size: 16)
        txtEmail.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
        txtEmail.text = "Username : "
        txtEmail.returnKeyType = .next
        txtEmail.tag = 0
        txtEmail.delegate = self
        txtEmail.autocorrectionType = .no
        txtEmail.autocapitalizationType = .none
        signInView.addSubview(txtEmail)
        
        txtPass.frame = CGRect(x: sideAlignment, y: 175, width: self.frame.width - sideAlignment * 2, height: 40)
        txtPass.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        txtPass.font = UIFont(name: "SFUIText-Medium", size: 16)
        txtPass.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
        txtPass.text = "Password : "
        txtPass.returnKeyType = .done
        txtPass.tag = 1
        txtPass.delegate = self
        signInView.addSubview(txtPass)
        
        btnSignIn.setTitle("Sign In", for: .normal)
        btnSignIn.setTitleColor(UIColor.white, for: .normal)
        btnSignIn.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnSignIn.titleLabel?.textAlignment = .center
        btnSignIn.sizeToFit()
        btnSignIn.frame = CGRect(x: 0, y: 250, width: btnSignIn.frame.width + 50, height: btnSignIn.frame.height + 10)
        btnSignIn.backgroundColor = UIColor(red: 0/255, green: 175/255, blue: 255/255, alpha: 1)
        btnSignIn.layer.cornerRadius = btnSignIn.frame.height / 2
        btnSignIn.center.x = self.frame.width / 2
        btnSignIn.addTarget(self, action: #selector(self.signedIn(_:)), for: .touchUpInside)
        signInView.addSubview(btnSignIn)
        
        let btnCopy = UIButton(type : .system)
        btnCopy.setTitle("Copy Device ID", for: .normal)
        btnCopy.setTitleColor(UIColor.black, for: .normal)
        btnCopy.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 18)
        btnCopy.titleLabel?.textAlignment = .center
        btnCopy.sizeToFit()
        btnCopy.center.x = self.frame.width / 2
        btnCopy.center.y = self.frame.height - 30
        btnCopy.addTarget(self, action: #selector(self.copyDeviceID(_:)), for: .touchUpInside)
        scrView.addSubview(btnCopy)
        
        signInView.frame.size.height = btnSignIn.frame.origin.y + btnSignIn.frame.height
        signInView.center.y = self.frame.height / 2
        scrView.addSubview(signInView)
        
        //Keyboard notification to listen to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        let userDefault = UserDefaults.standard
        print(userDefault.string(forKey: "password"))
        
    }
    
    func copyDeviceID(_ btn : UIButton)
    {
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print("device ID is \(deviceID)")
        
        UIPasteboard.general.string = deviceID
    }
    
    func automaticSignIn(vc : ViewController)
    {
        self.vc = vc
        self.userInform = vc.userInform
        self.backgroundColor = UIColor.clear
        
        let imgView = UIImageView(image : UIImage(named: "WaitLabel.png"))
        imgView.frame = CGRect(x: 0, y: 0, width: self.frame.width * 0.7, height: self.frame.width * 0.7)
        imgView.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        self.addSubview(imgView)
        
        automatic = true
        
        signIn()
        vc.loadingLabelUp(label : "Signing In..")
        
    }
    
    //to get the keyboard height--------------------
    func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardHeight = keyboardSize.height
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
                
                self.signInView.center.y = ((self.frame.height - keyboardHeight) / 2) + 20
                
            }, completion: nil)
            
        }
    }
    
    //ScrollView Delegate Methods----------------------------------------------------------
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.endEditing(true)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
            
            self.signInView.center.y = self.frame.height / 2
            
        }, completion: nil)
    }
    
    //TextField Delegate Methods-----------------------------------------------------------
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let userDefault = UserDefaults.standard
        
        if (textField.tag == 0 && textField.text == "Username : ")
        {
            txtEmail.text = userDefault.string(forKey: "userName")
        }
        else if (textField.tag == 1 && textField.text == "Password : ")
        {
            txtPass.isSecureTextEntry = true
            txtPass.text = ""
            if (txtEmail.text == userDefault.string(forKey: "userName"))
            {
                txtPass.text = userDefault.string(forKey: "password")
            }
            
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            
            textField.textColor = UIColor.black
            
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField.tag == 0)
        {
            txtPass.becomeFirstResponder()
        }
        else if (textField.tag == 1)
        {
            self.endEditing(true)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
                
                self.signInView.center.y = self.frame.height / 2
                
            }, completion: nil)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField.tag == 0)
        {
            if (textField.text == "" || textField.text == "Username : ")
            {
                textField.text = "Username : "
                textField.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
            }
        }
        else if (textField.tag == 1)
        {
            if (textField.text == "" || textField.text == "Password : ")
            {
                textField.text = "Password : "
                textField.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
                txtPass.isSecureTextEntry = false
            }
        }
        
    }
    
    //When Sign In Button is pressed-------------------------------------------------------
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    func signedIn(_ btn : UIButton)
    {
        btn.alpha = 0.5
        btn.isUserInteractionEnabled = false
        self.endEditing(true)
        signIn()
        vc.loadingLabelUp(label : "Signing In..")
    }
    
    func signIn()
    {
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        var parameters = [
            "email" : "\(txtEmail.text!)",
            "password" : "\(txtPass.text!)",
            "device_id" : deviceID
        ]
        
        if (firstTime)
        {
            parameters = [
                "email" : "\(txtEmail.text!)",
                "password" : "\(txtPass.text!)",
                "device_id" : deviceID
                
            ]
        }
        else
        {
            let userDefault = UserDefaults.standard
            parameters = [
                "email" : "\(userDefault.string(forKey: "userName")!)",
                "password" : "\(userDefault.string(forKey: "password")!)",
                "device_id" : deviceID
                
            ]
        }
        
        Alamofire.request("\(BASE_URL)employees/sign_in", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.api_succeed(JSON : JSON as! NSDictionary)
                    
                case .failure(let error) :
                    //                    self.signIn()
                    
                    if (!self.automatic)
                    {
                        self.vc.loadingView.failed()
                        self.btnSignIn.alpha = 1
                        self.btnSignIn.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.vc.loadingView.failed()
                        self.vc.signInView = SignInView()
                        self.vc.signInView.frame = self.vc.view.frame
                        self.vc.signInView.initialize(vc: self.vc)
                        self.vc.view.addSubview(self.vc.signInView)
                        
                        self.removeFromSuperview()
                        //                        self.signIn()
                    }
                    
                    print(" ERROR IS \(error)")
                }
        }
    }
    
    //*****************************************************************************
    //Warning : Please handle optional and don't make force unwrapping optional   *
    //*****************************************************************************
    func api_succeed(JSON : NSDictionary)
    {
        setupLeaveTypes()
        
        if (firstTime)
        {
            let userDefault = UserDefaults.standard
            userDefault.set("\(txtEmail.text!)", forKey: "userName")
            userDefault.set("\(txtPass.text!)", forKey: "password")
            userDefault.set(true, forKey: "signedIn")
        }
        
        if let idNum = JSON["id"] as? NSNumber
        {
            let idStr = "\(idNum)"
            userInform.id = idStr
        }
        
        if let auth_token = JSON["auth_token"] as? String,let name = JSON["name"] as? String, let email = JSON["email"] as? String, let eCode = JSON["employee_code"] as? String{
            userInform.auth_token = auth_token
            userInform.name = name
            userInform.email = email
            userInform.eCode = eCode
        }
        
        if let department : NSDictionary = JSON["department"] as? NSDictionary, let deptName = department["name"] as? String{
            userInform.department = deptName
        }
        
        if let bankAcc = JSON["bank_account"] as? String{
            userInform.bankAcc = bankAcc
        }
        
        if let position : NSDictionary = JSON["title"] as? NSDictionary{
            if let positionName = position["name"] as? String{
                userInform.position =  positionName
            }else{
                userInform.position = ""
            }
            
        }
        if let genderNum = JSON["gender"] as? NSNumber
        {
            if (genderNum == 1)
            {
                userInform.gender = "Male"
            }
            else
            {
                userInform.gender = "Female"
            }
            
        }
        
        if let dob = JSON["dob"] as? String{
            userInform.doB = dob
        }
        
        if let nrc = JSON["nrc"] as? String{
            userInform.nrc = nrc
        }
        
        if let address = JSON["address"] as? String{
            userInform.address = address
        }
        
        
        if let phone = JSON["phone"] as? [String]{
            userInform.phone = phone
        }
        
        if let emer = JSON["emergency_phones"] as? [NSDictionary]{
            for  i in (0 ..< emer.count)
            {
                userInform.emerName.append(emer[i]["name"] as! String)
                userInform.emerPhone.append(emer[i]["phone"] as! String)
                
                print("EmerName is \(userInform.emerName)")
                print("EmerPhone is \(userInform.emerPhone)")
            }
        }
        
        userInform.serLength = "not sure"
        if let salary = JSON["basic_pay"] as? String{
            userInform.salary = salary
        }
        
        vc.homeView = HomeView()
        vc.homeView.frame = self.vc.view.frame
        vc.homeView.name = userInform.name
        vc.homeView.email = userInform.email
        vc.homeView.initialize(self.vc)
        vc.homeView.alpha = 0
        vc.homeView.frame.origin.y = -self.frame.height
        vc.view.addSubview(self.vc.homeView)
        
        //Set the check type  number-------------------------
        let userDefault = UserDefaults.standard
        userDefault.set(1, forKey: "check_type")
        
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
            
            self.frame.origin.y = self.vc.view.frame.height
            self.vc.homeView.frame.origin.y = 0
            self.vc.homeView.alpha = 1
            
        }, completion: {finish in
            
            self.removeFromSuperview()
        })
        
        vc.loadingView.succeeded()
    }
    
    
    func setupLeaveTypes()
    {
        let auth = vc.userInform.auth_token
        Alamofire.request("\(BASE_URL)leave_types?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    self.leaveTypeJSON = JSON as! [NSDictionary]
                    print(JSON)
                    self.setupStatusTypes()
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    self.setupLeaveTypes()
                }
        }
    }
    
    func setupStatusTypes()
    {
        let auth = vc.userInform.auth_token
        Alamofire.request("\(BASE_URL)leave_statuses?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    self.statusJSON = JSON as! [NSDictionary]
                    print(JSON)
                    self.setupApproveBys()
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    self.setupStatusTypes()
                }
        }
    }
    
    func setupApproveBys()
    {
        let auth = vc.userInform.auth_token
        Alamofire.request("\(BASE_URL)approved_bys?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    self.approvedByJSON = JSON as! [NSDictionary]
                    print(JSON)
                    self.setupReportTos()
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    self.setupApproveBys()
                }
        }
    }
    
    func setupReportTos()
    {
        let auth = vc.userInform.auth_token
        Alamofire.request("\(BASE_URL)report_tos?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    self.reportToJSON = JSON as! [NSDictionary]
                    self.setupAdditionalProperties()
                    print(JSON)
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    self.setupReportTos()
                }
        }
    }
    
    //Setup Leave Types, Approve Bys, Report Tos and StatusTypes---------------------------
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    
    func setupAdditionalProperties()
    {
        //        signIn()
        
        self.vc.homeView.viewBtns[1].isUserInteractionEnabled = true
        self.vc.homeView.viewBtns[1].alpha = 1
        
        vc.userInform.leaveTypes.removeAll()
        vc.userInform.leaveTypesIDs.removeAll()
        vc.userInform.statusTypes.removeAll()
        vc.userInform.ApprovedByIDs.removeAll()
        vc.userInform.ApprovedByNames.removeAll()
        vc.userInform.ApprovedByJobs.removeAll()
        vc.userInform.ReportToIDs.removeAll()
        vc.userInform.ReportToNames.removeAll()
        vc.userInform.ReportToJobs.removeAll()
        
        for  i in ( 0 ..< leaveTypeJSON.count)
        {
            let type = leaveTypeJSON[i]
            vc.userInform.leaveTypes.append(type["name"] as! String)
            vc.userInform.leaveTypesIDs.append("\(type["id"] as! NSNumber)")
            
            if (type["name"] as! String == "Casual Leave")
            {
                vc.userInform.leaveColors[0] = UIColor(hexStr: type["color_code"] as! String, alpha: 1)
                print(UIColor.red)
            }
            else if (type["name"] as! String == "Annual Leave")
            {
                vc.userInform.leaveColors[1] = UIColor(hexStr: type["color_code"] as! String, alpha: 1)
                print(UIColor.green)
            }
            else if (type["name"] as! String == "Medical Leave")
            {
                vc.userInform.leaveColors[2] = UIColor(hexStr: type["color_code"] as! String, alpha: 1)
                print(UIColor.blue)
            }
            
        }
        for  i in ( 0 ..< statusJSON.count)
        {
            let type = statusJSON[i]
            vc.userInform.statusTypes.append(type["name"] as! String)
        }
        for  i in ( 0 ..< approvedByJSON.count)
        {
            let type = approvedByJSON[i]
            vc.userInform.ApprovedByIDs.append("\(type["id"] as! NSNumber)")
            vc.userInform.ApprovedByNames.append(type["name"] as! String)
            let role = type["role"] as! NSDictionary
            vc.userInform.ApprovedByJobs.append(role["name"] as! String)
        }
        for  i in ( 0 ..< reportToJSON.count)
        {
            let type = reportToJSON[i]
            vc.userInform.ReportToIDs.append("\(type["id"] as! NSNumber)")
            vc.userInform.ReportToNames.append(type["name"] as! String)
            let role = type["role"] as! NSDictionary
            vc.userInform.ReportToJobs.append(role["name"] as! String)
        }
    }
}
