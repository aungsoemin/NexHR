//
//  OvertimeViewForm.swift
//  Nex_HR
//
//  Created by Daud on 10/25/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class OvertimeFormView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var vc : ViewController!
    var sideAlignment : CGFloat = 20
    var scrollIndex = 0
    
    let headerCover = UIView()
    let scrView = UIScrollView()
    let tabView = UITableView()
    
    var txtColor : [UIColor] = [UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)]
    
//    let lblInit : [String] = ["Request Date","Employee_id","Evaluated Hours","Total Overtime","Status","Start Date","End Date","Project Name","Attachment","Approved By","Why Overtime is Required"]
    
    let lblInit : [String] = ["Start Date","End Date","Start Date Time","End Date Time","OT hours","Evaluated Hours","Project Name","Detail Tasks","Attachment","Approved By","Report To","Status","Reason"]
    
    var lblValues : [String] = ["Start Date","End Date","Start Date Time","End Date Time","OT hours","Evaluated Hours","Project Name","Detail Tasks","Attachment","Approved By","Report To","Status","Reason"]
    
    var curMonth = ["October","October"]
    var curYear = ["2016","2016"]
    var curYearNum = [0,0]
    var curMonthNum = [0,0]
    var curMonthIndex = [0,0]
    var curYearIndex = [0,0]
    var curDayNum = [0,0]
    var curDayIndex = [0,0]
    
    var curHourIndex = [11,11]
    var curMinuteIndex = [0,0]
    
    var curHour = ["12","12"]
    var curMinute = ["00","00"]
    
    var curStatus = "Casual Status"
    var curApprovedBy = "SomeOne"
    var curReportTo = "SomeOne"
    var curApprovedById = ""
    var curReportToId = ""
    
    var choosenImage = UIImage()
    
    var statusTypes = [String]()
    var approvedBys = [String]()
    var approvedByIDs = [String]()
    var reportTos = [String]()
    var reportToIDs = [String]()
    
    
//    var lbl2 : [String] = ["Request Date","Employee_id","Evaluated Hours","Total Overtime","Status","Start Date","End Date","Project Name","Attachment","Approved By","Why Overtime is Required"]
    
    func initialize(_ vc : ViewController!)
    {
        self.vc = vc
        self.backgroundColor = UIColor.white
        
        curMonthNum = [vc.userInform.curMonthNum,vc.userInform.curMonthNum]
        curYearNum = [vc.userInform.curYearNum,vc.userInform.curYearNum]
        curMonth = [vc.userInform.curMonth,vc.userInform.curMonth]
        curYear = [vc.userInform.curYear,vc.userInform.curYear]
        curMonthIndex = [vc.userInform.curMonthIndex,vc.userInform.curMonthIndex]
        curYearIndex = [vc.userInform.curYearIndex,vc.userInform.curYearIndex]
        curDayNum = [vc.userInform.curDayNum,vc.userInform.curDayNum]
        curDayIndex = [vc.userInform.curDayNum - 1, vc.userInform.curDayNum - 1]
        
        statusTypes = vc.userInform.statusTypes
        
        for var i in (0 ..< vc.userInform.ApprovedByNames.count)
        {
            approvedBys.append("\(vc.userInform.ApprovedByNames[i]) (\(vc.userInform.ApprovedByJobs[i]))")
        }
        approvedByIDs = vc.userInform.ApprovedByIDs
        
        for var i in (0 ..< vc.userInform.ReportToNames.count)
        {
            reportTos.append("\(vc.userInform.ReportToNames[i]) (\(vc.userInform.ReportToJobs[i]))")
        }
        reportToIDs = vc.userInform.ReportToIDs
        
        curStatus = statusTypes[0]
        curApprovedBy = approvedBys[0]
        curApprovedById = approvedByIDs[0]
        curReportTo = reportTos[0]
        curReportToId = reportToIDs[0]
        
        tabView.dataSource = self
        tabView.delegate = self
        tabView.register(SubmitCell.self, forCellReuseIdentifier: "submitCell")
        tabView.register(TextViewCell.self, forCellReuseIdentifier: "textViewCell")
        tabView.register(DatePickerCell.self, forCellReuseIdentifier: "datePickerCell")
        tabView.register(TimeCell.self, forCellReuseIdentifier: "timeCell")
        tabView.register(AttachmentCell.self, forCellReuseIdentifier: "attachmentCell")
        tabView.register(SelecterCell.self, forCellReuseIdentifier: "selecterCell")
        tabView.register(TextBoxCell.self, forCellReuseIdentifier: "textBoxCell")
        tabView.frame = CGRect(x: sideAlignment, y: 120, width: self.frame.width - sideAlignment * 2, height: self.frame.height - 120)
        tabView.separatorColor = UIColor.clear
        tabView.showsVerticalScrollIndicator = false
        tabView.delaysContentTouches = false
        self.addSubview(tabView)
        
        headerCover.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 1
        self.addSubview(headerCover)
        
        let btnCancle = UIButton(frame: CGRect(x: sideAlignment, y: 0, width: 100, height: 100))
        btnCancle.setTitle("Cancel", for: .normal)
        btnCancle.setTitleColor(UIColor(red : 0/255, green : 175/255, blue : 255/255, alpha : 1), for: .normal)
        btnCancle.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 18)
        btnCancle.sizeToFit()
        btnCancle.frame.origin.x = self.frame.width - btnCancle.frame.width - sideAlignment
        btnCancle.center.y = 50
        headerCover.addSubview(btnCancle)
        btnCancle.addTarget(self, action: #selector(self.goBack(_:)), for: .touchUpInside)
        
        let lblLeave = UILabel()
        lblLeave.text = "Overtime"
        lblLeave.textColor = UIColor.black
        lblLeave.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblLeave.sizeToFit()
        lblLeave.frame.origin.x = sideAlignment
        lblLeave.center.y = 50
        headerCover.addSubview(lblLeave)
        
        let lblDate = UILabel()
        lblDate.text = "2.9.2016"
        lblDate.textColor = UIColor.black
        lblDate.font = UIFont(name: "SFUIText-Regular", size: 15)
        lblDate.sizeToFit()
        lblDate.frame.origin.x = sideAlignment
        lblDate.center.y = 80
        headerCover.addSubview(lblDate)
        
        //Keyboard notification to listen to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    //to get the keyboard height--------------------
    func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardHeight = keyboardSize.height
            
            tabView.frame.size.height = frame.height - tabView.frame.origin.y - keyboardHeight
            UIView.animate(withDuration: 0, delay: 0.3, options: [], animations: {
                
                
            }, completion: {finish in
                
                self.tabView.scrollToRow(at: NSIndexPath(row: self.scrollIndex, section: 0) as IndexPath, at: .middle, animated: true)
            })
            
        }
    }
    
    //Scoll the tableCell to middle when textField is focused-----------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func keyboardWillHide(notification : NSNotification)
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            
            self.tabView.frame.size.height = self.frame.height - self.tabView.frame.origin.y
            
        }, completion: nil)
    }
    
    //Go back-----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func goBack(_ btn : UIButton!)
    {
        self.endEditing(true)
        vc.overtimeView.comeBack()
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.frame.height
            
            }, completion: {finish in
                
                self.removeFromSuperview()
                
        })
        
    }
    
    //Go back-----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------

    func submitForm()
    {
        vc.loadingLabelUp(label: "Sending..")
        
        let parameters = [
            "requested_date" : "\(vc.userInform.curYear)-\(vc.userInform.curMonthNum)-\(vc.userInform.curDayNum)",
            "employee_id" : "\(vc.userInform.id)",
            "start_date" : "\(curYear[0])/\(curMonthNum[0])/\(curDayNum[0]) \(curHour[0]):\(curMinute[0])",
            "end_date" : "\(curYear[1])/\(curMonthNum[1])/\(curDayNum[1]) \(curHour[1]):\(curMinute[1])",
            "ot_hours" : lblValues[4],
            "evaluated_hours" : lblValues[5],
            "project_name" : lblValues[6],
            "detail_tasks" : lblValues[7],
            "reason" : lblValues[12],
            "report_to_id" : curReportToId,
            "approved_by_id" : curApprovedById,
            "status" : "\(curStatus)"
        ]
        
        let image = choosenImage
        self.endEditing(true)

        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(UIImageJPEGRepresentation(image, 0.6)!, withName: "attachment", fileName: "img.jpeg", mimeType: "image/jpeg")
            
            for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        },
        to: "\(BASE_URL)overtimes?auth_token=\(vc.userInform.auth_token)",
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    
                    upload.responseJSON(completionHandler:
                        {response in
                            
                            print("response result = \(response.result)")
                            print("response data = \(response.data)")
                            
                            switch(response.result)
                            {
                            case .success(let JSON):
                                
                                print(JSON)
                                
                                self.vc.overtimeView.comeBack()
                                self.vc.loadingView.succeeded()
                                
                                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                                    
                                    self.frame.origin.y = self.frame.height
                                    
                                }, completion: {finish in
                                    
                                    self.removeFromSuperview()
                                    
                                })
                            
                            case .failure(let error):
                                print(error)
                                self.vc.loadingView.failed()
                            }
                    }
                    )
                    
                case .failure(let encodingError):
                    
                    print(encodingError)
                    self.vc.loadingView.failed()
                    
                }
        })
        
    }
    
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 14
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0 || indexPath.row == 1) //DatePickerCell
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "datePickerCell", for: indexPath as IndexPath) as! DatePickerCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : "\(curYear[indexPath.row]) \(curMonth[indexPath.row]) \(curDayNum[indexPath.row])", vc : self.vc, parentView : "overtimeView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 2 || indexPath.row == 3) //TimeCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell", for: indexPath as IndexPath) as! TimeCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : "\(curHour[indexPath.row - 2]) : \(curMinute[indexPath.row - 2])", vc : self.vc, parentView : "overtimeView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7) //TextBoxCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textBoxCell", for: indexPath as IndexPath) as! TextBoxCell
            cell.initialize(vc : self.vc, lbl2 : lblValues[indexPath.row], parentClass : "overtimeView", cellNum : indexPath.row, txtColor : txtColor[indexPath.row])
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 8) //AttachmentCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "attachmentCell", for: indexPath as IndexPath) as! AttachmentCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : "choose", vc : self.vc, parentView : "overtimeView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 9) //SelectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : curApprovedBy, vc : self.vc, parentView : "overtimeView", cellNum : indexPath.row, leaveList : approvedBys, label : "Who Approved You?", idList : approvedByIDs)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 10) //SelectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : curReportTo, vc : self.vc, parentView : "overtimeView", cellNum : indexPath.row, leaveList : reportTos, label : "Who Was Reported?", idList : reportToIDs)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 11) //SelectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : curStatus, vc : self.vc, parentView : "overtimeView", cellNum : indexPath.row, leaveList : statusTypes, label : "Choose Status Type", idList : [])
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 12) //TextViewCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath as IndexPath) as! TextViewCell
            cell.initialize(vc : self.vc, lbl2 : lblValues[indexPath.row], parentClass : "overtimeView", cellNum : indexPath.row, txtColor : txtColor[indexPath.row])
            cell.selectionStyle = .none
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell", for: indexPath as IndexPath) as! SubmitCell
            cell.initialize(vc : self.vc, parentClass : "overtimeView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 13)
        {
            return 140
        }
        else if(indexPath.row == 12)
        {
            return 100
        }
        else
        {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.endEditing(true)
    }


}
