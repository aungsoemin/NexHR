//
//  checkBoxCell.swift
//  Nex_HR
//
//  Created by Daud on 11/16/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class HalfDayCell: UITableViewCell {
    
    let label = UILabel()
    let btn1 = UIButton()
    let btn2 = UIButton()
    let label1 = UIButton()
    let label2 = UIButton()
    var chosenType = 0
    
    var vc : ViewController!
    var parentView = ""
    var cellNum = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style : style, reuseIdentifier : reuseIdentifier)
        self.addSubview(label)
        self.addSubview(btn1)
        self.addSubview(label1)
        self.addSubview(btn2)
        self.addSubview(label2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(lbl : String, halfDayType : String, vc : ViewController, parentView : String)
    {
        self.vc = vc
        self.parentView = parentView
        
        label.frame.size.width = 1000
        label.text = lbl
        label.textColor = UIColor.black
        label.font = UIFont(name: "SFUIText-Medium", size: 16)
        label.sizeToFit()
        label.frame.origin.x = 0
        label.frame.origin.y = 5
        label.alpha = 0.7
        
        btn1.backgroundColor = UIColor.clear
        btn1.frame = CGRect(x: 0, y: 35, width: 22, height: 22)
        btn1.layer.cornerRadius = btn1.frame.width / 2
        btn1.layer.borderColor = UIColor.black.cgColor
        btn1.layer.borderWidth = 2
        btn1.addTarget(self, action: #selector(self.chooseType(_:)), for: .touchUpInside)
        
        label1.frame = CGRect(x: 30, y: 0, width: 1000, height: 20)
        label1.setTitle("Morning", for: .normal)
        label1.setTitleColor(UIColor.black, for: .normal)
        label1.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 16)
        label1.sizeToFit()
        label1.center.y = btn1.center.y
        label1.addTarget(self, action: #selector(self.chooseType(_:)), for: .touchUpInside)
        
        btn2.backgroundColor = UIColor.clear
        btn2.frame = CGRect(x: label1.frame.origin.x + label1.frame.width + 20, y: 35, width: 22, height: 22)
        btn2.layer.cornerRadius = btn2.frame.width / 2
        btn2.layer.borderColor = UIColor.black.cgColor
        btn2.layer.borderWidth = 2
        btn2.addTarget(self, action: #selector(self.chooseType(_:)), for: .touchUpInside)
        
        label2.frame = CGRect(x: btn2.frame.origin.x + btn2.frame.width + 5, y: 0, width: 1000, height: 20)
        label2.setTitle("Evening", for: .normal)
        label2.setTitleColor(UIColor.black, for: .normal)
        label2.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 16)
        label2.sizeToFit()
        label2.center.y = btn2.center.y
        label2.addTarget(self, action: #selector(self.chooseType(_:)), for: .touchUpInside)
    }
    
    func chooseType(_ btn : UIButton)
    {
        if (btn == btn1 || btn == label1)
        {
            if (chosenType == 2 || chosenType == 0)
            {
                btn2.setImage(nil, for: .normal)
                btn1.setImage(UIImage (named : "btnTick.png"), for: .normal)
                chosenType = 1
                vc.leaveFormView.isHalfDay = "1"
//                vc.leaveFormView.endDateCell.alpha = 0.4
//                vc.leaveFormView.endDateCell.isUserInteractionEnabled = false
            }
            else
            {
                btn1.setImage(nil, for: .normal)
                chosenType = 0
                vc.leaveFormView.isHalfDay = "0"
//                vc.leaveFormView.endDateCell.alpha = 1
//                vc.leaveFormView.endDateCell.isUserInteractionEnabled = true
            }
            
        }
        else if (btn == btn2 || btn == label2)
        {
            if (chosenType == 1 || chosenType == 0)
            {
                btn1.setImage(nil, for: .normal)
                btn2.setImage(UIImage (named : "btnTick.png"), for: .normal)
                chosenType = 2
                vc.leaveFormView.isHalfDay = "1"
//                vc.leaveFormView.endDateCell.alpha = 0.4
//                vc.leaveFormView.endDateCell.isUserInteractionEnabled = false
            }
            else
            {
                btn2.setImage(nil, for: .normal)
                chosenType = 0
                vc.leaveFormView.isHalfDay = "0"
//                vc.leaveFormView.endDateCell.alpha = 1
//                vc.leaveFormView.endDateCell.isUserInteractionEnabled = true
            }
        }
        
        vc.leaveFormView.halfDayType = chosenType
//        print("\(vc.leaveFormView.halfDayType)")
        vc.leaveFormView.tabView.reloadData()
    }

}
