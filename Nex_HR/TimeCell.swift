//
//  TimeCell.swift
//  Nex_HR
//
//  Created by Daud on 11/15/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class TimeCell: UITableViewCell {
    
    let lbl = UILabel()
    let btn = UIButton()
    
    var vc : ViewController!
    var parentView = ""
    var cellNum = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style : style, reuseIdentifier : reuseIdentifier)
        self.addSubview(lbl)
        self.addSubview(btn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(lbl2 : String, btn2 : String, vc : ViewController, parentView : String, cellNum : Int)
    {
        self.vc = vc
        self.parentView = parentView
        self.cellNum = cellNum
        print(cellNum)
        
        lbl.frame.size.width = 1000
        lbl.text = lbl2
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "SFUIText-Medium", size: 16)
        lbl.sizeToFit()
        lbl.frame.origin.x = 0
        lbl.center.y = self.frame.height / 2
        
        btn.setTitle(btn2, for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 15)
        btn.sizeToFit()
        btn.backgroundColor = UIColor(red: 0, green: 175/255, blue: 255/255, alpha: 1)
        btn.frame = CGRect(x: lbl.frame.width + 10, y: 0, width: btn.frame.width + 20, height: btn.frame.height + 5)
        btn.layer.cornerRadius = 5
        btn.center.y = self.frame.height / 2
        btn.addTarget(self, action: #selector(self.chooseDate(_:)), for: .touchUpInside)
        btn.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btn.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btn.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
    }
    
    func chooseDate(_ btn : UIButton)
    {
        vc.timePicker = TimePicker()
        vc.timePicker.frame = vc.view.frame
        vc.timePicker.initialize(vc, parentView : parentView, cellNum : cellNum)
        vc.view.addSubview(vc.timePicker)
    }

}
