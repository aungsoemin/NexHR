//
//  TableCell5.swift
//  Nex_HR
//
//  Created by Daud on 11/7/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell, UITextViewDelegate {

    let txt = UITextView()
    var vc : ViewController!
    var parentClass = ""
    var cellNum = 0
    var txtColor = UIColor()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style : style, reuseIdentifier : reuseIdentifier)
        txt.font = UIFont(name: "SFUIText-Medium", size: 16)
        txt.delegate = self
        self.addSubview(txt)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(vc : ViewController, lbl2 : String, parentClass : String, cellNum : Int, txtColor : UIColor)
    {
        self.vc = vc
        self.parentClass = parentClass
        self.cellNum = cellNum
        self.txtColor = txtColor
        
        txt.text = lbl2
        txt.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        txt.returnKeyType = .next
        txt.center.y = self.frame.height / 2
        txt.textColor = txtColor
        
        let borderView = UIView(frame: CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: 1))
        borderView.backgroundColor = UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1)
        self.addSubview(borderView)
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (parentClass == "leaveView")
        {
            vc.leaveFormView.scrollIndex = self.cellNum
            if(txt.text == vc.leaveFormView.lblInit[cellNum])
            {
                txt.text = ""
                vc.leaveFormView.txtColor[cellNum] = UIColor.black
                txt.textColor = vc.leaveFormView.txtColor[cellNum]
            }
            
        }
        else if (parentClass == "overtimeView")
        {
            vc.overtimeFormView.scrollIndex = self.cellNum
            if(txt.text == vc.overtimeFormView.lblInit[cellNum])
            {
                txt.text = ""
                vc.overtimeFormView.txtColor[cellNum] = UIColor.black
                txt.textColor = vc.overtimeFormView.txtColor[cellNum]
            }
        }
        else if (parentClass == "manualView")
        {
            if(txt.text == vc.manualView.reasonInit)
            {
                txt.text = ""
                txt.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if (parentClass == "leaveView")
        {
            vc.leaveFormView.curReason = txt.text!
        }
        else if (parentClass == "overtimeView")
        {
            vc.overtimeFormView.lblValues[cellNum] = txt.text!
        }
        else if (parentClass == "manualView")
        {
            vc.manualView.curReason = txt.text!
        }
    }
    
//    func txtChanged()
//    {
//        if (parentClass == "leaveView")
//        {
//            vc.leaveFormView.curReason = txt.text!
//        }
//        else if (parentClass == "overtimeView")
//        {
//            vc.overtimeFormView.lblValues[cellNum] = txt.text!
//        }
//    }
    

}
