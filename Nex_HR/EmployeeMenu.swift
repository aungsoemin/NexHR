//
//  EmployeeTableView.swift
//  Nex_HR
//
//  Created by Daud on 4/9/17.
//  Copyright © 2017 Daud. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class EmployeeMenu : UIView, UIScrollViewDelegate
{
    var vc : ViewController!
    let sideAlignment : CGFloat = 20
    
    var json : [NSDictionary] = []
    var index = 0
    
    var goBackState = "state1"
    var forceCancel = false
    var previousOffsetY : CGFloat = 0
    var autoGoBackReady = false
    var offsetY : CGFloat = 200
    
    let scrView = UIScrollView()
    let headerCover = UIView()
    let lblEmployee = UIButton()
    var borders : [UIView] = []
    
    var links : [String] = []
    var names : [String] = []
    
    func initialize(_ vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = UIColor.clear
        
        scrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        scrView.delegate = self
        scrView.delaysContentTouches = false
        scrView.showsVerticalScrollIndicator = false
        scrView.contentSize.height = self.frame.height + 1
        self.addSubview(scrView)
        
        lblEmployee.setTitle("Handbook", for: .normal)
        lblEmployee.setTitleColor(UIColor.black, for: .normal)
        lblEmployee.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblEmployee.sizeToFit()
        lblEmployee.frame.origin.x = sideAlignment
        lblEmployee.center.y = 50
        scrView.addSubview(lblEmployee)
        
        let btnHome = UIButton(frame: CGRect(x: self.frame.width - sideAlignment - 35, y: sideAlignment , width: 35, height: 35))
        btnHome.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnHome.center.y = 50
        btnHome.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        scrView.addSubview(btnHome)
        
        
        headerCover.frame = CGRect(x: 0, y: -20, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 0
        self.addSubview(headerCover)
        
        let lblEmployee2 = UILabel()
        lblEmployee2.text = "Handbook"
        lblEmployee2.textColor = UIColor.black
        lblEmployee2.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblEmployee2.sizeToFit()
        lblEmployee2.frame.origin.x = sideAlignment
        lblEmployee2.center.y = headerCover.frame.height / 2
        headerCover.addSubview(lblEmployee2)
        
        let btnHome2 = UIButton(frame: CGRect(x: self.frame.width - 35 - sideAlignment, y: sideAlignment , width: 35, height: 35))
        btnHome2.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome2.imageView?.contentMode = .scaleAspectFit
        btnHome2.center.y = headerCover.frame.height / 2
        btnHome2.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        headerCover.addSubview(btnHome2)
        
        let lblBookList = UILabel(frame : CGRect(x: sideAlignment, y: 130, width: 100, height: 100))
        lblBookList.text = "List of Employee Handbooks"
        lblBookList.textColor = UIColor.black
        lblBookList.font = UIFont(name: "SFUIText-Medium", size: 18)
        lblBookList.sizeToFit()
        scrView.addSubview(lblBookList)
        
        self.autoGoBackReady = true
        api_Post()
        
    }
    
    func goBackHome(_ btn : UIButton)
    {
        vc.homeView.viewBtns[4].alpha = 1
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.vc.homeView.btnBorders[4].alpha = 1
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
        self.vc.homeView.comeBack(delay : 0.15)
    }
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func api_Post()
    {
        self.vc.loadingLabelUp(label: "Requesting..")
        let auth = vc.userInform.auth_token
        
//        let parameters : NSDictionary = [:]
        
//        Alamofire.request("\(BASE_URL)employee_handbooks?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
        Alamofire.request("\(BASE_URL)employee_handbooks?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.vc.loadingView.succeeded()
//                    self.json = JSON
                    self.arrangeButtons(json: JSON as! [NSDictionary])
                    
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                }
        }
        
    }

    //Arrange buttons---------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func arrangeButtons(json : [NSDictionary])
    {
        for jj in json
        {
            links.append(jj["file"] as! String)
            names.append(jj["name"] as! String)
            
            let btn = UIButton(type : .system)
            btn.frame = CGRect(x: sideAlignment, y: offsetY, width: self.frame.width - sideAlignment, height: 20)
            btn.contentHorizontalAlignment = .left
            btn.setTitle(jj["name"] as! String, for: .normal)
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 22)
            btn.sizeToFit()
            btn.frame.size.width = self.frame.width - sideAlignment
            btn.tintColor = UIColor.white
            btn.tag = index
            btn.addTarget(self, action: #selector(self.handbookPressed(_:)), for: .touchUpInside)
            scrView.addSubview(btn)
            
            offsetY = offsetY + 65
            index = index + 1
            
            let borderLine = UIView(frame : CGRect(x: sideAlignment, y: btn.frame.origin.y + 45, width: self.frame.width - sideAlignment, height: 1))
            borderLine.backgroundColor = UIColor(red: 240/255, green : 240/255, blue : 240/255, alpha : 1)
            scrView.addSubview(borderLine)
            borders.append(borderLine)
            
            scrView.contentSize.height = borderLine.frame.origin.y
            if(scrView.contentSize.height < self.frame.height)
            {
                scrView.contentSize.height = self.frame.height + 1
            }
        }
    }
    
    func handbookPressed(_ btn : UIButton)
    {
        vc.statusBarHidden = true
        
        self.vc.employeeHandBook = EmployeeHandbook()
        self.vc.view.addSubview(self.vc.employeeHandBook)
        self.vc.employeeHandBook.frame = self.vc.view.frame
        self.vc.employeeHandBook.frame.origin.y = self.vc.view.frame.height
        self.vc.employeeHandBook.initialize(vc: vc)

        for cc in self.borders{
            cc.alpha = 0
        }
        
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.vc.homeView.alpha = 0
            self.vc.view.backgroundColor = UIColor.black
            self.frame.origin.y = -self.vc.view.frame.height
            self.vc.employeeHandBook.frame.origin.y = 0
            
        },completion : { finish in
            
            self.vc.employeeHandBook.getPDF(requestedURL: self.links[btn.tag])
            
        })
    }
    
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrOffset = scrView.contentOffset.y
        
        if (scrOffset > previousOffsetY)
        {
            forceCancel = true
        }
        else if (scrOffset < previousOffsetY)
        {
            forceCancel = false
        }
        
        previousOffsetY = scrOffset
        
        if (scrOffset >= 110)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 1
                self.headerCover.frame.origin.y = 0
                
            }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 0
                self.headerCover.frame.origin.y = -20
                
            }, completion: nil)
        }
        
        if (scrOffset <= -0 && (goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            self.transform = CGAffineTransform(scaleX: 1 + ((scrOffset + 0) * 0.0015), y: 1 + ((scrOffset + 0) * 0.0015))
            self.center.y = self.vc.view.frame.height / 2 - (scrOffset + 0) * 0.5
        }
        else if((goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.center.y = self.vc.view.frame.height / 2
                
            }, completion: nil)
        }
        
        if (scrOffset <= -80 && goBackState == "state1" && autoGoBackReady)
        {
            goBackState = "state2"
            vc.viewSwitchLabel.showFromAbove(text: "release to go back")
        }
        else if (scrOffset >= -80 && goBackState == "state2" && autoGoBackReady)
        {
            goBackState = "state1"
            vc.viewSwitchLabel.hide()
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (goBackState == "state2" && !forceCancel && autoGoBackReady)
        {
            goBackState = "state3"
            vc.viewSwitchLabel.hide()
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.alpha = 0
                self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                self.frame.origin.y = self.vc.view.frame.height
                
            }, completion: {finish in
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.vc.homeView.viewBtns[4].alpha = 1
                })
                self.vc.homeView.comeBack(delay : 0)
                self.vc.homeView.btnBorders[4].alpha = 1
                self.removeFromSuperview()
                
            })
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        forceCancel = false
    }
    
}
