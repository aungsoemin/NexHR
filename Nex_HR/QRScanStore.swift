//
//  QRScanStore.swift
//  Nex_HR
//
//  Created by Daud on 4/7/17.
//  Copyright © 2017 Daud. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Alamofire

class QRScan : Object
{
    dynamic var code = ""
    dynamic var dateTime = ""
}

class QRScanStore
{
//    var vc : ViewController!
    
    func storeData(code : String, dateTime : String)
    {
        
        print("Hello")
        
        let realm = try! Realm()
        
        let qrScan = QRScan()
        qrScan.code = code
        qrScan.dateTime = dateTime
        
        try! realm.write {
            realm.add(qrScan)
        }
        
        let qrScans = realm.objects(QRScan.self)
        
        for qq in qrScans
        {
            print(qq.code)
            print(qq.dateTime)
        }
    }
    
    func clearData()
    {
        let realm = try! Realm()
        
        try! realm.write {
            let qrScans = realm.objects(QRScan.self)
            realm.delete(qrScans)
        }
    }
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func uploadScans(vc : ViewController, qr : QRView)
    {
        
        let auth = vc.userInform.auth_token
        
        let realm = try! Realm()
        let qrScans = realm.objects(QRScan.self)
        
        var pp = Array<Any>()
        
        for qq in qrScans
        {
            print("code is \(qq.code)")
            print("date time is \(qq.dateTime)")
            
            let combined = [
                "code" : "\(qq.code)",
                "type" : "\(UserDefaults.standard.integer(forKey: "check_type"))",
                "date_time" : "\(qq.dateTime)"
            ]
            
            pp.append(combined)
        }
        
        vc.loadingLabelUp(label: "Uploading Scans..")
        
        //Serialize the String : Any to text
        
        let data = try! JSONSerialization.data(withJSONObject: pp, options: .prettyPrinted)
        
        let params = ["check_values" : String(data: data, encoding: .utf8)!]
        
        print("parameter is \(params)")
        
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)/qr_check?auth_token=\(auth)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type" : "application/json"])
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    vc.loadingView.succeeded()
                    self.clearData()
                    qr.btnBack.alpha = 1
                    qr.goBack(UIButton())
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    vc.loadingView.failed()
                    qr.btnBack.alpha = 1
                }
        }        
    }
    
}




