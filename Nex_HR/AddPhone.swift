//
//  AddPhone.swift
//  Nex_HR
//
//  Created by Daud on 11/21/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class AddPhone: UIView, UIGestureRecognizerDelegate, UITextFieldDelegate {

    var sideAlignment = CGFloat(20)
    
    var vc : ViewController!
    var parentView = ""
    
    let txtContainer = UIView()
    let txtName = UITextField()
    let txtPhone = UITextField()
    
    
    let btnAdd = UIButton(type : .system)
    
    var editing = false
    
    func initialize(_ vc : ViewController, parentView : String)
    {
        self.vc = vc
        self.parentView = parentView
        
        self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.goBack(_:)))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
        txtContainer.backgroundColor = UIColor.clear
        txtContainer.frame = CGRect(x: self.frame.width / 2, y: self.frame.height, width: self.frame.width - sideAlignment * 2, height: 200)
        txtContainer.alpha = 0
        self.addSubview(txtContainer)
        
        let lbl = UILabel(frame : CGRect(x: 0, y: 0, width: 0, height: 0))
        lbl.text = "Add a phone number"
        lbl.textAlignment = .center
        lbl.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
        lbl.font = UIFont(name: "SFUIText-Regular", size: 18)
        lbl.sizeToFit()
        txtContainer.addSubview(lbl)
        
        if (parentView == "Emergency")
        {
            txtName.frame = CGRect(x: 0, y: 50, width: txtContainer.frame.width * 0.4 - 5, height: 40)
            txtName.text = "Name"
            txtName.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
            txtName.font = UIFont(name: "SFUIText-Bold", size: 25)
            txtName.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
            txtName.isUserInteractionEnabled = true
            txtName.returnKeyType = .next
            txtName.delegate = self
            txtContainer.addSubview(txtName)
            
            txtPhone.frame = CGRect(x: txtContainer.frame.width * 0.4 + 5, y: 50, width: txtContainer.frame.width * 0.6 - 5, height: 40)
            
        }
        else
        {
            txtPhone.frame = CGRect(x: 0, y: 50, width: txtContainer.frame.width, height: 40)
        }
        
        txtPhone.text = "Phone"
        txtPhone.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        txtPhone.font = UIFont(name: "SFUIText-Bold", size: 25)
        txtPhone.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
        txtPhone.isUserInteractionEnabled = true
        txtPhone.returnKeyType = .done
        txtPhone.delegate = self
        txtContainer.addSubview(txtPhone)
        
        btnAdd.setTitle("Add Phone", for: .normal)
        btnAdd.setTitleColor(UIColor.black, for: .normal)
        btnAdd.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnAdd.sizeToFit()
        btnAdd.frame.origin.y = 150
        btnAdd.frame.origin.x = 0
        btnAdd.addTarget(self, action: #selector(self.api_Post(_:)), for: .touchUpInside)
        btnAdd.tintColor = UIColor.white
        txtContainer.addSubview(btnAdd)
        
        txtContainer.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.vc.homeView.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            
            self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            self.vc.profileView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            self.txtContainer.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.txtContainer.center = CGPoint(x : self.frame.width / 2 , y : self.frame.height / 2)
            self.txtContainer.alpha = 1
            
        }, completion: nil)
        
        //Keyboard notification to listen to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
    }
    
    //to get the keyboard height--------------------
    func keyboardWillShow(notification: NSNotification)
    {
        editing = true
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardHeight = keyboardSize.height
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
                
                self.txtContainer.center.y = ((self.frame.height - keyboardHeight) / 2) + 20
                
            }, completion: nil)
            
        }
    }
    
    //Gesture View Delegate Methods-------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view != self && touch.view != txtContainer) {
            return false
        }
        else
        {
            return true
        }
    }
    
    func goBack(_ gesture : UIGestureRecognizer)
    {
        if (editing)
        {
            self.endEditing(true)
            editing = false
        }
        else
        {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.alpha = 0
                self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                self.vc.profileView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.removeFromSuperview()
            })
        }
    }
    
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func api_Post(_ btn : UIButton)
    {
        self.endEditing(true)
        self.vc.loadingLabelUp(label: "Adding..")
        
        btnAdd.isUserInteractionEnabled = false
        btnAdd.alpha = 0.4
        
        let auth = vc.userInform.auth_token
        
        var tempPhone = self.vc.userInform.phone
        var tempEmerName = self.vc.userInform.emerName
        var tempEmerPhone = self.vc.userInform.emerPhone
        
        if (self.parentView == "Phone")
        {
            tempPhone.append(txtPhone.text!)
        }
        else if(self.parentView == "Emergency")
        {
            tempEmerName.append(txtName.text!)
            tempEmerPhone.append(txtPhone.text!)
        }
        
        vc.loadingLabelUp(label : "Updating Info..")
        
        let dd = "Value"
        
        var tempEp : [[String : String]] = []
        
        for var i in(0 ..< tempEmerName.count)
        {
            let ep : [String : String] = [
                "name" : tempEmerName[i],
                "phone" : tempEmerPhone[i]
            ]
            tempEp.append(ep)
        }
        
        let uv : [String: Any] = [
            "address" : vc.userInform.address,
            "phone" : tempPhone,
            "emergency_phone" : tempEp
        ]
        
        //Serialize the String : Any into text-------------------
        let data = try! JSONSerialization.data(withJSONObject: uv, options: .prettyPrinted)
        
        let parameters = [
            "update_values" : String(data: data, encoding: .utf8)!
        ]
        
        print(parameters)
        
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)?auth_token=\(vc.userInform.auth_token)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type" : "application/json"])
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    
//                    self.reloadProfile(JSON: JSON as! NSDictionary)
                    
                    self.vc.userInform.phone = tempPhone
                    self.vc.userInform.emerName = tempEmerName
                    self.vc.userInform.emerPhone = tempEmerPhone
                    
                    self.vc.profileView.updateInformation()
                    self.vc.profileView.tabView.reloadData()
                    
                    self.editing = false
                    self.goBack(UIGestureRecognizer())
                    
                    self.vc.loadingView.succeeded()
                    
                    
                case .failure(let error) :
                    
                    self.vc.loadingView.failed()
                    
                    self.btnAdd.isUserInteractionEnabled = true
                    self.btnAdd.alpha = 1
                }
        }
        
    }
    
//    func reloadProfile(JSON : NSDictionary)
//    {
//        if (parentView == "Emergency")
//        {
//            let lists = JSON["emergency_phones"] as! [NSDictionary]
//            vc.userInform.emerContact.removeAll()
//            
//            for var i in (0 ..< lists.count)
//            {
//                vc.userInform.emerContact.append("\(lists[i]["name"] as! String) : \(lists[i]["phone"] as! String)")
//            }
//            
//            vc.profileView.emerList = vc.userInform.emerContact
//        }
//        else
//        {
//            vc.userInform.phone = JSON["phones"] as! [String]
//        }
//        
//    }
    
    
    //TextField Delegate Methods-----------------------------------------------------------
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField == txtName && txtName.text == "Name")
        {
            txtName.text = ""
            txtName.font = UIFont(name: "SFUIText-Bold", size: 18)
            txtName.textColor = UIColor.black
        }
        else if (textField == txtPhone && txtPhone.text == "Phone")
        {
            txtPhone.text = ""
            txtPhone.font = UIFont(name: "SFUIText-Bold", size: 18)
            txtPhone.textColor = UIColor.black
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == txtName)
        {
            txtPhone.becomeFirstResponder()
        }
        else if (textField == txtPhone)
        {
            self.endEditing(true)
            api_Post(UIButton())
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
                
                self.txtContainer.center.y = self.frame.height / 2
                
            }, completion: nil)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField == txtName)
        {
            if (textField.text == "")
            {
                txtName.text = "Name"
                txtName.font = UIFont(name: "SFUIText-Bold", size: 25)
                txtName.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
            }
        }
        else if (textField == txtPhone)
        {
            if (textField.text == "")
            {
                txtPhone.text = "Phone"
                txtPhone.font = UIFont(name: "SFUIText-Bold", size: 25)
                txtPhone.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
            }
        }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
            
            self.txtContainer.center.y = self.frame.height / 2
            
        }, completion: nil)

        
    }

}
