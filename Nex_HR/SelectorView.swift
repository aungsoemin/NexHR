//
//  SelecterView.swift
//  Nex_HR
//
//  Created by Daud on 11/11/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class SelectorView: UIView,UIGestureRecognizerDelegate {

    var vc : ViewController!
    var cellNum : Int!
    var parentView = ""
    
    let selectorArea = UIView()
    
    var idList = [String]()
    
    func initialize(vc : ViewController, cellNum : Int, label : String, list : [String], parentView : String, idList : [String])
    {
        self.vc = vc
        self.cellNum = cellNum
        self.parentView = parentView
        self.idList = idList
        
        self.frame = vc.view.frame
        self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        
        selectorArea.frame = CGRect(x: 0, y: vc.view.frame.height, width: vc.view.frame.width, height: 50)
        selectorArea.backgroundColor = UIColor.white
        self.addSubview(selectorArea)
        
        let lbl = UILabel(frame : CGRect(x: 0, y: 20, width: 0, height: 0))
        lbl.text = label
        lbl.textAlignment = .center
        lbl.textColor = UIColor.black
        lbl.alpha = 0.4
        lbl.font = UIFont(name: "SFUIText-Bold", size: 15)
        lbl.sizeToFit()
        lbl.center.x = self.frame.width / 2
        selectorArea.addSubview(lbl)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTapped(_:)))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
        for var i in (0 ..< list.count)
        {
            
            let btn = UIButton(frame : CGRect(x: 0, y: 60 + (45 * i), width: Int(self.frame.width), height: 30))
            btn.setTitle(list[i], for: .normal)
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 18)
            btn.addTarget(self, action: #selector(self.pickType(_:)), for: .touchUpInside)
            btn.layer.addBorder(.bottom, color: UIColor(red: 203/255, green : 203/255, blue : 203/255, alpha : 0), thickness: 1)
            btn.tag = i
            selectorArea.addSubview(btn)
            
            selectorArea.frame.size.height = btn.frame.origin.y + btn.frame.height + 20
        }
        
        if (parentView == "leaveView")
        {
            self.vc.leaveView.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height - self.selectorArea.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08)
                self.vc.leaveFormView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
                
            }, completion: nil)
        }
        else if (parentView == "overtimeView")
        {
            self.vc.overtimeView.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height - self.selectorArea.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08)
                self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
                
            }, completion: nil)
        }
        else if (parentView == "manualView")
        {
//            self.vc.manualView.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height - self.selectorArea.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08)
                self.vc.manualView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
                
            }, completion: nil)
        }
        
        
    }
    
    func pickType(_ btn : UIButton)
    {
        if (parentView == "leaveView")
        {
            if (cellNum == 3)
            {
                vc.leaveFormView.curLeaveType = (btn.titleLabel?.text)!
                vc.leaveFormView.curLeaveTypeId = "\(idList[btn.tag])"
            }
            else if(cellNum == 5)
            {
                vc.leaveFormView.curApprovedBy = (btn.titleLabel?.text)!
                vc.leaveFormView.curApprovedById = idList[btn.tag]
            }
            else if(cellNum == 6)
            {
                vc.leaveFormView.curReportTo = (btn.titleLabel?.text)!
                vc.leaveFormView.curReportToId = idList[btn.tag]
            }
            vc.leaveFormView.tabView.reloadData()
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.vc.leaveFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.vc.leaveView.alpha = 1
                self.removeFromSuperview()
            })
        }
        else if (parentView == "overtimeView")
        {
            if (cellNum == 9)
            {
                vc.overtimeFormView.curApprovedBy = (btn.titleLabel?.text)!
                vc.overtimeFormView.curApprovedById = idList[btn.tag]
            }
            else if(cellNum == 10)
            {
                vc.overtimeFormView.curReportTo = (btn.titleLabel?.text)!
                vc.overtimeFormView.curReportToId = idList[btn.tag]
            }
            else if(cellNum == 11)
            {
                vc.overtimeFormView.curStatus = (btn.titleLabel?.text)!
            }
            vc.overtimeFormView.tabView.reloadData()
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.vc.overtimeView.alpha = 1
                self.removeFromSuperview()
            })
        }
        else if (parentView == "manualView")
        {
            vc.manualView.curApprovedBy = (btn.titleLabel?.text)!
            vc.manualView.curApprovedById = idList[btn.tag]
            print("curApprovedBy is \(vc.manualView.curApprovedById)")
            
            vc.manualView.tabView.reloadData()
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.vc.manualView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.vc.manualView.alpha = 1
                self.removeFromSuperview()
            })
        }
        
    }
    
    //Cancle Picking----------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func backgroundTapped(_ gesture : UITapGestureRecognizer)
    {
        if (parentView == "leaveView")
        {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.vc.leaveFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.vc.leaveView.alpha = 1
                self.removeFromSuperview()
            })
            
        }else if(parentView == "overtimeView")
        {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.vc.overtimeView.alpha = 1
                self.removeFromSuperview()
            })
            
        }else if(parentView == "manualView")
        {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
                
                self.selectorArea.frame.origin.y = self.vc.view.frame.height
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.vc.manualView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }, completion: {finish in
                self.vc.manualView.alpha = 1
                self.removeFromSuperview()
            })
            
        }
    }
    
    //Gesture View Delegate Methods-------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view != self) {
            return false
        }
        else
        {
            return true
        }
    }

}
