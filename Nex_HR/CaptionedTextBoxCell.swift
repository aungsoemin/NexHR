//
//  CaptionedTextBoxCell.swift
//  Nex_HR
//
//  Created by Daud on 9/29/16.
//  Copyright © 2016 Daud. All rights reserved.
//

//A Label and a textbox----------

import UIKit

class CaptionedTextBoxCell: UITableViewCell {
    
    func initialize(_ lbl1 : String, lbl2 : String)
    {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 30))
        lbl.font = UIFont(name: "SFUIText-Medium", size: 18)
        lbl.textColor = UIColor.black
        lbl.alpha = 0.5
        lbl.text = lbl1
        self.addSubview(lbl)
        
        let txt = UITextField(frame: CGRect(x: 0, y: 30, width: self.frame.width, height: 40))
        txt.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        txt.font = UIFont(name: "SFUIText-Medium", size: 18)
        txt.textColor = UIColor.black
        txt.text = lbl2
        txt.isUserInteractionEnabled = false
        self.addSubview(txt)
        
    }
    
}
