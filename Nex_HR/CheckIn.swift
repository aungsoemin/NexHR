//
//  CheckIn.swift
//  Nex_HR
//
//  Created by Daud on 11/18/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import Foundation
import SystemConfiguration.CaptiveNetwork
import UIKit

class CheckIn : UIView, UIGestureRecognizerDelegate
{
    
    var vc : ViewController!
    
    var btnQr = UIButton()
    var btnManual = UIButton()
    
    func initialize(vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = UIColor.white
        
        let userDefault = UserDefaults.standard
        
        let date = Date()
        let previousDate = userDefault.object(forKey: "previousDate") as? Date
        var hour = Calendar.current.component(.hour, from: date)
        hour = hour - 12
        
        print("hour is \(hour)")
        
        if (hour >= 3)
        {
            userDefault.set(2, forKey: "check_type")
        }
        else
        {
            print("hello")
            if (previousDate != nil)
            {
                print("today Date is \(date)")
                print("previous Date is \(previousDate)")
                
                if Calendar.current.compare(date, to: previousDate!, toGranularity: .day) == .orderedDescending {
                    userDefault.set(1, forKey: "check_type")
                }
                else
                {
                    userDefault.set(2, forKey: "check_type")
                }
//
            }
            else
            {
                userDefault.set(1, forKey: "check_type")
            }
        }
        
        if (userDefault.integer(forKey: "check_type") == 1)
        {
            btnQr.setTitle("QR Scan Check In", for: .normal)
            btnManual.setTitle("Manual Check In", for: .normal)
        }
        else if (userDefault.integer(forKey: "check_type") == 2)
        {
            btnQr.setTitle("QR Scan Check Out", for: .normal)
            btnManual.setTitle("Manual Check Out", for: .normal)
        }
        btnQr.setTitleColor(UIColor.black, for: .normal)
        btnQr.titleLabel?.textAlignment = .center
        btnQr.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnQr.sizeToFit()
        btnQr.center.x = self.frame.width / 2
        btnQr.center.y = self.frame.height / 2 - 25
        btnQr.addTarget(self, action: #selector(self.qrTapped(_:)), for: .touchUpInside)
        self.addSubview(btnQr)
        
        btnManual.setTitleColor(UIColor.black, for: .normal)
        btnManual.titleLabel?.textAlignment = .center
        btnManual.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnManual.sizeToFit()
        btnManual.center.x = self.frame.width / 2
        btnManual.center.y = self.frame.height / 2 + 25
        btnManual.addTarget(self, action: #selector(self.manualTapped(_:)), for: .touchUpInside)
        self.addSubview(btnManual)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTapped(_:)))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
        self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0,options: [], animations: {
            
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
            
        }, completion: nil)
        
    }
    
    //Gesture recognizer delegate method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view == self) {
            return true
        }
        else
        {
            return false
        }
    }
    
    //Tapp To Go Back-----------------
    func backgroundTapped(_ gesture : UITapGestureRecognizer)
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.alpha = 0
            self.vc.homeView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.vc.homeView.alpha = 1
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
    }
    
    //QR View ----------------------------------------------------
    func qrTapped(_ btn : UIButton)
    {
        let qrView = QRView()
        qrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.addSubview(qrView)
        qrView.initialize(vc: self.vc)
        
//        QRScanStore().storeData(code: "code", dateTime: "date")
    }
    
    //Manual View ----------------------------------------------------
    func manualTapped(_ btn : UIButton)
    {
        
        vc.manualView = ManualView()
        vc.manualView.frame = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: self.frame.height)
        self.vc.view.addSubview(vc.manualView)
        
        UIView.animate(withDuration: 0.3, delay: 0 , options: [], animations: {
            
            self.vc.manualView.frame.origin.y = 0
            self.frame.origin.y = -20
            
        })
        
        vc.manualView.initialize(vc: self.vc)
        
    }
    
    func comeBack()
    {
        UIView.animate(withDuration: 0.3, delay: 0 , options: [], animations: {
            
            self.frame.origin.y = 0
            
        })
    }
    
    
//    func getSSID() -> String? {
//        
//        let interfaces = CNCopySupportedInterfaces()
//        if interfaces == nil {
//            return "no interface"
//        }
//        
//        let interfacesArray = interfaces as! [String]
//        if interfacesArray.count <= 0 {
//            return "No interfaceArray"
//        }
//        
//        let interfaceName = interfacesArray[0] as String
//        let unsafeInterfaceData =     CNCopyCurrentNetworkInfo(interfaceName as CFString)
//        if unsafeInterfaceData == nil {
//            return "No unsafeInterfaceData"
//        }
//        
//        let interfaceData = unsafeInterfaceData as! Dictionary <String,AnyObject>
//        
//        return interfaceData["SSID"] as? String
//    }
}
